//// Utilities for rendering HTML templates.

import gleam/string_builder.{StringBuilder}
import glentities
import glemplate/renderer
import glemplate/ast
import glemplate/assigns

/// Encode content as HTML entities so it's safe in an HTML template.
///
/// Note! This is not safe for use in `<script>` or `<style>` tags!
pub fn encode(text: String) -> StringBuilder {
  glentities.encode(text, glentities.HTMLBody)
  |> string_builder.from_string()
}

/// Render an HTML template.
pub fn render(
  template: ast.Template,
  assigns: assigns.Assigns,
  template_cache: renderer.TemplateCache,
) -> renderer.RenderResult {
  renderer.render(
    template,
    assigns,
    renderer.RenderOptions(encoder: encode, template_cache: template_cache),
  )
}
