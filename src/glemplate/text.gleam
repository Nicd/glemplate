//// Utilities for rendering plain text templates.

import gleam/string_builder
import glemplate/renderer
import glemplate/ast
import glemplate/assigns

/// An encoder that just returns the content as-is.
pub fn encode(str: String) {
  string_builder.from_string(str)
}

/// Render a plain text template.
pub fn render(
  template: ast.Template,
  assigns: assigns.Assigns,
  template_cache: renderer.TemplateCache,
) -> renderer.RenderResult {
  renderer.render(
    template,
    assigns,
    renderer.RenderOptions(encoder: encode, template_cache: template_cache),
  )
}
