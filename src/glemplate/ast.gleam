//// The abstract syntax tree that templates are stored as in memory. This AST
//// can be rendered with the renderer module.

pub type Node {
  Text(String)
  Dynamic(DynamicNode)
  Nodes(nodes: NodeList)
}

pub type NodeList =
  List(Node)

pub type Template {
  Template(name: TemplateName, nodes: NodeList)
}

/// A dynamic node is used for dynamic behaviour in a template.
pub type DynamicNode {
  /// Output the variable, encoded.
  Output(Var)

  /// Output the variable, without encoding.
  RawOutput(Var)

  /// Render a node list based on if the assign is truthy (non-false).
  If(Var, if_true: NodeList, if_false: NodeList)

  /// Iterate over the variable, bind it to a new name, and render the node list
  /// for every item.
  Iter(over: Var, binding: VarName, NodeList)

  /// Render the given template, using the assigns mapping. The assigns mapping
  /// is a list of tuples `(from, to)`, where the `from` named assigns are
  /// available with `to` names in the child template.
  Render(tpl: TemplateName, assigns_map: List(#(Var, VarName)))
}

/// A reference to a template with the given name.
pub type TemplateName =
  String

/// A reference to a variable input.
pub type Var {
  /// Reference to an assign with the given name.
  Assign(name: VarName)

  /// Accessing a field of an associative container.
  FieldAccess(container: Var, field: String)

  /// Accessing an item in an indexable container.
  IndexAccess(container: Var, index: Int)
}

pub type VarName =
  String
