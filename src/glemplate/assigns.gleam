//// Assigns are used to insert dynamic data into a template. This module
//// contains convenience functions for creating assigns, but you may also
//// create the map manually with Gleam's map functions if you so wish.

import gleam/map.{Map}
import glemplate/ast.{VarName}

/// Data in an assign. Note that only `String` and `Int` are stringifiable
/// values.
pub type AssignData {
  String(String)
  Int(Int)

  /// A function to execute that returns assign data on demand.
  Lazy(LazyFn)
  Bool(Bool)
  Map(Map(VarName, AssignData))
  List(List(AssignData))
}

/// Lazy function that should return assign data when executed.
pub type LazyFn =
  fn() -> AssignData

/// Assigns given to a template for rendering dynamic content.
pub type Assigns =
  Map(VarName, AssignData)

pub fn new() -> Assigns {
  map.new()
}

pub fn from_list(list: List(#(VarName, AssignData))) -> Assigns {
  map.from_list(list)
}

pub fn add_string(assigns: Assigns, name: VarName, value: String) {
  map.insert(assigns, name, String(value))
}

pub fn add_int(assigns: Assigns, name: VarName, value: Int) {
  map.insert(assigns, name, Int(value))
}

pub fn add_lazy(assigns: Assigns, name: VarName, value: LazyFn) {
  map.insert(assigns, name, Lazy(value))
}

pub fn add_bool(assigns: Assigns, name: VarName, value: Bool) {
  map.insert(assigns, name, Bool(value))
}

pub fn add_list(assigns: Assigns, name: VarName, value: List(AssignData)) {
  map.insert(assigns, name, List(value))
}

pub fn add_map(assigns: Assigns, name: VarName, value: Map(VarName, AssignData)) {
  map.insert(assigns, name, Map(value))
}
