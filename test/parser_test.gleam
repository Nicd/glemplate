import gleeunit/should
import glemplate/parser
import glemplate/ast.{
  Assign, Dynamic, FieldAccess, If, IndexAccess, Iter, Nodes, Output, RawOutput,
  Render, Text,
}

pub fn output_test() {
  let template = "<%= aurora %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [Nodes([]), Text(""), Dynamic(Output(Assign("aurora"))), Text("")],
  )
}

pub fn raw_output_test() {
  let template = "<%= raw aurora %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [Nodes([]), Text(""), Dynamic(RawOutput(Assign("aurora"))), Text("")],
  )
}

pub fn if_test() {
  let template = "<% if martin %>Hello, I'm Martin<% end %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [
      Nodes([]),
      Text(""),
      Dynamic(If(Assign("martin"), [Text("Hello, I'm Martin")], [])),
      Text(""),
    ],
  )
}

pub fn if_else_test() {
  let template =
    "<% if martin %>Hello, I'm Martin<% else %>Sorry, I think you've mistaken me for someone else<% end %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [
      Nodes([]),
      Text(""),
      Dynamic(If(
        Assign("martin"),
        [Text("Hello, I'm Martin")],
        [Text("Sorry, I think you've mistaken me for someone else")],
      )),
      Text(""),
    ],
  )
}

pub fn for_test() {
  let template = "<% for item in shopping %>- <%= item %><% end %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [
      Nodes([]),
      Text(""),
      Dynamic(Iter(
        Assign("shopping"),
        "item",
        [Text("- "), Dynamic(Output(Assign("item"))), Text("")],
      )),
      Text(""),
    ],
  )
}

pub fn render_test() {
  let template = "<% render foo %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [Nodes([]), Text(""), Dynamic(Render("foo", [])), Text("")],
  )
}

pub fn render_1_binding_test() {
  let template = "<% render foo a: b %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [
      Nodes([]),
      Text(""),
      Dynamic(Render("foo", [#(Assign("b"), "a")])),
      Text(""),
    ],
  )
}

pub fn render_n_bindings_test() {
  let template = "<% render foo a: b, c: d, e: f %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [
      Nodes([]),
      Text(""),
      Dynamic(Render(
        "foo",
        [#(Assign("b"), "a"), #(Assign("d"), "c"), #(Assign("f"), "e")],
      )),
      Text(""),
    ],
  )
}

pub fn field_access_test() {
  let template = "<%= aurora.conqueror %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [
      Nodes([]),
      Text(""),
      Dynamic(Output(FieldAccess(Assign("aurora"), "conqueror"))),
      Text(""),
    ],
  )
}

pub fn index_access_test() {
  let template = "<%= aurora.0 %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [
      Nodes([]),
      Text(""),
      Dynamic(Output(IndexAccess(Assign("aurora"), 0))),
      Text(""),
    ],
  )
}

pub fn chained_access_test() {
  let template = "<%= aurora.albums.0.publish_year %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [
      Nodes([]),
      Text(""),
      Dynamic(Output(FieldAccess(
        IndexAccess(FieldAccess(Assign("aurora"), "albums"), 0),
        "publish_year",
      ))),
      Text(""),
    ],
  )
}

pub fn raw_field_access_test() {
  let template = "<%= raw aurora.conqueror %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [
      Nodes([]),
      Text(""),
      Dynamic(RawOutput(FieldAccess(Assign("aurora"), "conqueror"))),
      Text(""),
    ],
  )
}

pub fn if_field_access_test() {
  let template = "<% if user.is_martin %>Hello, I'm Martin<% end %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [
      Nodes([]),
      Text(""),
      Dynamic(If(
        FieldAccess(Assign("user"), "is_martin"),
        [Text("Hello, I'm Martin")],
        [],
      )),
      Text(""),
    ],
  )
}

pub fn for_access_test() {
  let template = "<% for item in user.shopping_lists.0 %>- <%= item %><% end %>"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [
      Nodes([]),
      Text(""),
      Dynamic(Iter(
        IndexAccess(FieldAccess(Assign("user"), "shopping_lists"), 0),
        "item",
        [Text("- "), Dynamic(Output(Assign("item"))), Text("")],
      )),
      Text(""),
    ],
  )
}

pub fn complex_test() {
  let template =
    "<h1>HIGH SCORES:</h1>
<ul>
  <% for score in scores %>
    <li>
      <% if score %>
        <% render score_tpl.html.glemp score: score %>
      <% end %>
    </li>
  <% end %>
</ul>

You can create comments by using comment tags: <%%!-- Comment --%>

<%!-- The pyramid: --%>
<% if user %>
  <% if other_user %>
    <% if third_user %>
      <% for item in user.items %>
        - User's item: <%= item %>
        <% for ou_item in other_user.items %>
          - Other user's item: <%= ou_item %>
          <% for tu_item in third_user.items %>
            - Third user's item: <%= tu_item %>

            <% render item_combos.html.glemp item_1: item, item_2: ou_item, item_3: tu_item %>
          <% end %>
        <% end %>
      <% end %>
    <% else %>
      Third user not found!
    <% end %>
  <% else %>
    Second user not found!
  <% end %>
<% else %>
  User not found!
<% end %>
"

  let assert Ok(parsed) = parser.parse(template)
  should.equal(
    parsed,
    [
      Nodes([]),
      Text("<h1>HIGH SCORES:</h1>\n<ul>\n  "),
      Dynamic(Iter(
        Assign("scores"),
        "score",
        [
          Text("\n    <li>\n      "),
          Dynamic(If(
            Assign("score"),
            [
              Text("\n        "),
              Dynamic(Render(
                "score_tpl.html.glemp",
                [#(Assign("score"), "score")],
              )),
              Text("\n      "),
            ],
            [],
          )),
          Text("\n    </li>\n  "),
        ],
      )),
      Text("\n</ul>\n\nYou can create comments by using comment tags: "),
      Text("<%"),
      Text("!-- Comment --%>\n\n"),
      Text("\n"),
      Dynamic(If(
        Assign("user"),
        [
          Text("\n  "),
          Dynamic(If(
            Assign("other_user"),
            [
              Text("\n    "),
              Dynamic(If(
                Assign("third_user"),
                [
                  Text("\n      "),
                  Dynamic(Iter(
                    FieldAccess(Assign("user"), "items"),
                    "item",
                    [
                      Text("\n        - User's item: "),
                      Dynamic(Output(Assign("item"))),
                      Text("\n        "),
                      Dynamic(Iter(
                        FieldAccess(Assign("other_user"), "items"),
                        "ou_item",
                        [
                          Text("\n          - Other user's item: "),
                          Dynamic(Output(Assign("ou_item"))),
                          Text("\n          "),
                          Dynamic(Iter(
                            FieldAccess(Assign("third_user"), "items"),
                            "tu_item",
                            [
                              Text("\n            - Third user's item: "),
                              Dynamic(Output(Assign("tu_item"))),
                              Text("\n\n            "),
                              Dynamic(Render(
                                "item_combos.html.glemp",
                                [
                                  #(Assign("item"), "item_1"),
                                  #(Assign("ou_item"), "item_2"),
                                  #(Assign("tu_item"), "item_3"),
                                ],
                              )),
                              Text("\n          "),
                            ],
                          )),
                          Text("\n        "),
                        ],
                      )),
                      Text("\n      "),
                    ],
                  )),
                  Text("\n    "),
                ],
                [Text("\n      Third user not found!\n    ")],
              )),
              Text("\n  "),
            ],
            [Text("\n    Second user not found!\n  ")],
          )),
          Text("\n"),
        ],
        [Text("\n  User not found!\n")],
      )),
      Text("\n"),
    ],
  )
}
