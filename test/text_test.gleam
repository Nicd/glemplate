import gleam/map
import gleam/string_builder
import gleeunit/should
import glemplate/parser
import glemplate/text
import glemplate/assigns

pub fn encoding_test() {
  let template = "**Welcome, <%= name %>!** <%= raw name %>"

  let assert Ok(tpl) = parser.parse_to_template(template, "input.txt.glemp")

  let template_cache = map.new()
  let assigns =
    assigns.new()
    |> assigns.add_string("name", "||<xXx_KillaBoy_69>||")

  let assert Ok(result) = text.render(tpl, assigns, template_cache)

  should.equal(
    string_builder.to_string(result),
    "**Welcome, ||<xXx_KillaBoy_69>||!** ||<xXx_KillaBoy_69>||",
  )
}
